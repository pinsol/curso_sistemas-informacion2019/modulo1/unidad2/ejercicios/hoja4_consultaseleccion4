﻿/** MODULO I - UNIDAD II - CONSULTAS DE SELECCION 4 **/

USE ciclistas;

/* 1.1 Nombre y edad de los ciclistas que han ganado etapas */
SELECT
  c.nombre,
  c.edad
FROM
  (SELECT DISTINCT
    e.dorsal
   FROM etapa e) c1
    JOIN ciclista c USING (dorsal)
;

/* 1.2 Nombre y edad de los ciclistaws que han ganado puertos */
SELECT
  c.nombre,
  c.edad
FROM
  (SELECT DISTINCT
    p.dorsal
   FROM puerto p) c1
    JOIN ciclista c USING (dorsal)
;

/* 1.3 Nombre y edad de los ciclistas que han gando etapas y puertos */
SELECT
  c.nombre,
  c.edad 
FROM
  ciclista c 
  JOIN (SELECT DISTINCT  e.dorsal FROM etapa e) c1 USING (dorsal) 
  JOIN (SELECT DISTINCT p.dorsal FROM puerto p) c2 USING (dorsal)
;

/* 1.4 Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa */
    SELECT * FROM etapa e;
    SELECT * FROM equipo e;
    SELECT * FROM ciclista c;

  -- dorsales de ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;
  -- equipos con ciclistas que han ganado etapas
SELECT DISTINCT c.nomequipo FROM ciclista c JOIN (
  SELECT DISTINCT e.dorsal FROM etapa e
  ) c1 USING (dorsal)
;

-- SOLUCIÓN:
SELECT
  e.director
FROM
  equipo e JOIN (
    SELECT DISTINCT c.nomequipo FROM ciclista c JOIN (
      SELECT DISTINCT e.dorsal FROM etapa e
      ) c1 USING (dorsal)
  ) c2 USING (nomequipo)
;

/* 1.5 Dorsal y nombre de los ciclistas que hayan llevado algún maillot */
    SELECT * FROM lleva l;

  -- dorsales de los ciclistas que han llevado maillots
  SELECT DISTINCT l.dorsal FROM lleva l;

-- SOLUCIÓN:
SELECT
  c1.dorsal,
  c.nombre 
FROM
  ciclista c JOIN (
    SELECT DISTINCT l.dorsal FROM lleva l
    ) c1 USING(dorsal)
;

/* 1.6 Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo */
    SELECT * FROM lleva l;
    SELECT * FROM maillot m;

  -- codido del maillot amarillo
    SELECT m.código FROM maillot m WHERE color LIKE 'amarillo';

  -- dorsales de los ciclistas que han llevado maillots amarillos
    SELECT DISTINCT dorsal FROM lleva JOIN (
      SELECT m.código FROM maillot m WHERE color LIKE 'amarillo'
      ) c1 USING(código);

-- SOLUCIÓN:
SELECT
  c.dorsal,
  c.nombre 
FROM
  ciclista c JOIN (
    SELECT DISTINCT dorsal FROM lleva JOIN (
        SELECT m.código FROM maillot m WHERE color LIKE 'amarillo'
        ) c1 USING(código)
    ) c2 USING (dorsal)
;

/* 1.7 Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas */
    SELECT * FROM lleva l;
    SELECT * FROM etapa e;

  -- dorsales de ciclistas que han llevado maillot
    SELECT DISTINCT l.dorsal FROM lleva l;

-- SOLUCIÓN:
SELECT
  DISTINCT e.dorsal 
FROM
  etapa e JOIN (
    SELECT DISTINCT l.dorsal FROM lleva l
  ) c1 USING (dorsal)
;

/* 1.8 Indicar el numetapa de las etapas que tengan puertos */
SELECT
  DISTINCT p.numetapa 
FROM
  puerto p
;

/* 1.9 Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos */
    SELECT * FROM ciclista c; -- dorsales de banesto
    SELECT * FROM puerto p; -- numetapas con puertos
    SELECT * FROM etapa e;  -- kms de las etapas

  -- dorsales de los ciclistas de Banesto
  SELECT c.dorsal FROM ciclista c WHERE c.nomequipo LIKE 'BANESTO';
  -- numetapas de los puertos ganados por ciclistas de Banesto
  SELECT DISTINCT p.numetapa FROM puerto p JOIN (
    SELECT c.dorsal FROM ciclista c WHERE c.nomequipo LIKE 'BANESTO'
  ) c1 USING (dorsal);

-- SOLUCIÓN:
SELECT
  e.kms 
FROM
  etapa e JOIN (
    SELECT DISTINCT p.numetapa FROM puerto p JOIN (
      SELECT c.dorsal FROM ciclista c WHERE c.nomequipo LIKE 'BANESTO'
    ) c1 USING (dorsal)
  ) c2 USING (numetapa)
;

/* 1.10 Listar el número de ciclistas que hayan ganado alguna etapa con puerto */
    SELECT * FROM etapa e;  -- count dorsales ganadores de etapa
    SELECT * FROM puerto p; -- numetapa con puertos

  -- numetapas con puertos
  SELECT p.numetapa FROM puerto p;
  
-- SOLUCIÓN:
SELECT
  COUNT(DISTINCT e.dorsal) nGanadoresEtapaConPuerto 
FROM
  etapa e JOIN (
    SELECT p.numetapa FROM puerto p
  ) c1 USING (numetapa)
;

/* 1.11 Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto */
    SELECT * FROM ciclista c; -- dorsales de ciclistas de Banesto
    SELECT * FROM puerto p; -- nombre de los puertos con los dorsales de los ganadores de los ciclistas de Banesto

  -- dorsales de los ciclistas de Banesto
    SELECT c.dorsal FROM ciclista c WHERE c.nomequipo LIKE 'BANESTO';

-- SOLUCIÓN:
SELECT
  p.nompuerto 
FROM
  puerto p JOIN (
    SELECT c.dorsal FROM ciclista c WHERE c.nomequipo LIKE 'BANESTO'
  ) c1 USING (dorsal)
;

/* 1.12 Listar el número de etapas que tengan puerto que hayan sido ganados por ciclistas de Banesto con mas de 200 km */
    SELECT * FROM ciclista c; -- dorsales de los ciclistas de Banesto
    SELECT * FROM puerto p; -- numetapa mediante dorsales de Banesto
    SELECT * FROM etapa e;  -- count* where kms>200

  -- dorsales de Banesto
  SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.nomequipo LIKE 'BANESTO';
  -- etapas con puerto ganados por Banesto
  SELECT DISTINCT p.numetapa FROM puerto p JOIN (
    SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.nomequipo LIKE 'BANESTO'
  ) c1 USING (dorsal);
  
-- SOLUCIÓN:
SELECT
  COUNT(*) total 
FROM
  etapa e JOIN (
      SELECT DISTINCT p.numetapa FROM puerto p JOIN (
      SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.nomequipo LIKE 'BANESTO'
    ) c1 USING (dorsal)  
  ) c2 USING (numetapa) 
WHERE
  e.kms > 200
;